﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Bwin.DomainClasses.Base;
using Bwin.DomainClasses.Users;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Bwin.DomainClasses.Messages
{
    public class Topic:BaseEntity
    {
        /// <summary>
        /// who triggered the topic 
        /// </summary>
        [ForeignKey("UserId")]
        public  User User { get; set; }
        public  int UserId { get; set; }
        [StringLength(50)]
        public  string Subject { get; set; }
        /// <summary>
        /// topic is closed or not
        /// </summary>
        public  bool Closed { get; set; }

        /// <summary>
        /// true means and admin responded last time , in other case user sent message
        /// </summary>
        public bool IsAdminLastReponder { get; set; }

        public ICollection<Message> Messages { get; set; }

    }
}
