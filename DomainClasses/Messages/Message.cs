﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Bwin.DomainClasses.Base;
using Bwin.DomainClasses.Users;

namespace Bwin.DomainClasses.Messages
{
    public class Message:BaseEntity
    {
        [ForeignKey("TopicId")]
        public Topic Topic { get; set; }
        public int TopicId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        public int? UserId { get; set; }

        [StringLength(2000)]
        public string MessageText { get; set; }

     

    }
}
