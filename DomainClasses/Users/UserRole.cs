﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bwin.DomainClasses.Users
{
    public class UserRole
    {
        [Key]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }
        [Key]
        public Role Role { get; set; }
    }
}
