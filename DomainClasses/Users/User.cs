﻿using Plus.Utilities.Core.Financial;
using System;
using System.ComponentModel.DataAnnotations;

namespace Bwin.DomainClasses.Users
{
    public class User
    {

        // ===== Info
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public bool Sex { get; set; }
        [StringLength(150)]
        public string Address { get; set; }
        [StringLength(10)]
        public string PostCode { get; set; }
        [StringLength(15)]
        public string PhoneNumber { get; set; }
        /// <summary>
        /// include 0
        /// </summary>
        [StringLength(11)]
        public string CellPhone { get; set; }


        // ====================Authentication 
        [StringLength(15)]
        public string Username { get; set; }

        /// <summary>
        /// limit it to 15 chars and keep this len to fill up with hashed phrase
        /// </summary>
        [StringLength(100)]
        public string Password { get; set; }


        // ====== Monetary 
        [StringLength(20)]
        public string AccountNumber { get; set; }
        [StringLength(50)]
        public string AccountOwner { get; set; }

        /// <summary>
        /// card number are 16 digits or 20 digits
        /// </summary>
        [StringLength(20)]
        public string CardNumber { get; set; }
        public Bank BankType { get; set; }

        /// <summary>
        /// the whole money the user have in his account
        /// </summary>
        public long Repository { get; set; }

        /// <summary>
        /// 0 means behave on general settings
        /// more than that user cant request money to pay him as much as we defined here
        /// </summary>
        public int MaxRequestToPay { get; set; }

        


        //====== status and modes

        public DateTime LastSeen { get; set; }
        public UserStatus Status { get; set; }

        /// <summary>
        /// this is an option description will be filled in by admin
        /// </summary>
        [StringLength(200)]
        public string Description { get; set; }

        /// <summary>
        /// behave  to the user as if there is no such user
        /// </summary>
        public bool IsRemoved { get; set; }


    }
}
