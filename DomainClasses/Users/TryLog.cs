﻿using Bwin.DomainClasses.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bwin.DomainClasses.Users
{
    public class TryLog:LogBase
    {
        [StringLength(15)]
        public string UserName { get; set; }
        [StringLength(15)]
        public string Password { get; set; }
    }
}
