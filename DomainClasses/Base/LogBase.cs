﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bwin.DomainClasses.Base
{
    public class LogBase:BaseEntity
    {
        [StringLength(15)]
        public String IP { get; set; }
        [StringLength(50)]
        public string BrowserName { get; set; }
        [StringLength(50)]
        public string BrowserVersion { get; set; }
        [StringLength(50)]
        public string BrowserVersionMajor { get; set; }
        [StringLength(50)]
        public string OSName { get; set; }
        [StringLength(50)]
        public string OSVersion { get; set; }
    }
}
