﻿using Plus.Utilities.Core.Perisan.Date;
using System;
using System.ComponentModel.DataAnnotations;

namespace Bwin.DomainClasses.Base
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            InsertTime = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }

        private DateTime _insertTime;

        public DateTime InsertTime
        {
            get { return _insertTime; }
            set
            {
                _insertTime = value;
                PersianInsertTime = value.ToPersianString(PersianDateTimeFormat.Date);
            }
        }
        [StringLength(10)]
        public string PersianInsertTime { get; set; }
        [StringLength(10)]
        public string PersianModifiedTime { get; set; }

        private DateTime? _modifiedTime;

        public DateTime? ModifiedTime
        {
            get { return _modifiedTime; }
            set
            {
                _modifiedTime = value;
                if(value.HasValue)
                    PersianInsertTime = value.Value.ToPersianString(PersianDateTimeFormat.Date);
            }
        }

    }
}
