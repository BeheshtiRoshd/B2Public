﻿using System.ComponentModel.DataAnnotations.Schema;
using Bwin.DomainClasses.Games;
using Bwin.Components.Bets;

namespace Bwin.DomainClasses.Games.Basket
{
    public class GameItem
    {
        [ForeignKey("BasketId")]
        public Basket Basket { get; set; }
        public int BasketId { get; set; }

        [ForeignKey("GameId")]
        public Game Game { get; set; }
        public int GameId { get; set; }

        /// <summary>
        /// what kind of bet is chosen
        /// </summary>
        public BetKind BetKind { get; set; }

        /// <summary>
        /// set index or value of the chosen Bet
        /// </summary>
        public int TipIndex { get; set; }

        /// <summary>
        /// coeffecient of ChosenTip
        /// </summary>
        public decimal CoEffecient { get; set; }

        public BasketStatus Status { get; set; }

        public void CheckResult(int gameId)
        {

            //is that the game is gonna check?
            if (GameId != gameId)
                    return;

                //if this bet was checked before?
                if (Status != BasketStatus.Netural)
                    return;
                Basket.GameItemCheckedCount++;
                //if your here so this game is chosen by user
                //get ticket from betcreator to check bet was corrected or not?
                var ticket = BetCreator.GetTicket(BetKind);
                var result = ticket.RunCheckers(this);
                if (result)
                {
                    //user was right so this bet was winner
                    Status = BasketStatus.Winner;

                    return;
                }
                //oh shit user was not right so he was wrong
                Status = BasketStatus.Loser;

            
        }
    }
}
