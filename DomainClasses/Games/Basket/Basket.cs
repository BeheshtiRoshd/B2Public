﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Bwin.DomainClasses.Users;
using Bwin.Components.Bets;

namespace Bwin.DomainClasses.Games.Basket
{
    public class Basket
    {
        [ForeignKey("UserId")]
        public User User { get; set; }
        public  int UserId { get; set; }


        /// <summary>
        /// how many games the user picked up
        /// </summary>
        public int GameItemCount { get; set; }
        public int GameItemCheckedCount { get; set; }
       
        public BasketStatus Status { get; set; }

        public virtual ICollection<GameItem> GameItems { get; set; }

        /// <summary>
        /// sum of gameitems coeffecient
        /// </summary>
        public decimal TotalCoeffecient { get; set; }

        
        /// <summary>
        /// the amount user did a bet on it
        /// </summary>
        public int BetAmount { get; set; }
      
    }
}
