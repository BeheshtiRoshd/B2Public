﻿using Bwin.DomainClasses.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bwin.DomainClasses.Games
{
    public class League:BaseEntity
    {
        [StringLength(70)]
            public string Name { get; set; }
            public int RemoteId { get; set; }
    }
}
