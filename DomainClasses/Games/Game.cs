﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Plus.Utilities.Core.Perisan.Date;

namespace Bwin.DomainClasses.Games
{
    public class Game
    {

        public League League { get; set; }
        [ForeignKey("HostId")]
        public Team Host { get; set; }
        public int? HostId { get; set; }
        [ForeignKey("GuestId")]
        public Team Guest { get; set; }
        public int? GuestId { get; set; }

        /// <summary>
        /// what kind of game is
        /// </summary>
        public GameType GameType { get; set; }

        /// <summary>
        /// count of scores or goal host did
        /// </summary>
        public int HostScore{ get; set; }

        /// <summary>
        /// count of scores or goal Guest did
        /// </summary>
        public int GuestScore{ get; set; }

        /// <summary>
        /// when the game starts data and time
        /// </summary>
        public DateTime StartTime { get; set; }

        public GameStatus Status { get;set; }


        /// <summary>
        /// its good to show more excitment features to know what is live status of the game by hunch of system
        /// </summary>
        /// <returns></returns>
        public GameStatus GetExactGameStatus()
        {
            var directReturns = new[]
            {
                GameStatus.GameIsCancelled,
                GameStatus.GameIsAbort,
                GameStatus.GameIsAccomplished,

            };

            if (directReturns.Contains(Status))
                return Status;

            var isPassed=StartTime.IsPassed();
            if (isPassed)
            {
                if (StartTime.IsWithin(StartTime, 120))
                {
                    return GameStatus.GameIsRunnig;
                }
            }
            var howMuchIsRemianToStart = DateTime.Now - StartTime;
            if (howMuchIsRemianToStart.Minutes <= 30)
            {
                return GameStatus.GameIsAboutToStart;
            }

            if (StartTime.IsPassed(120))
            {
                return GameStatus.GameIsFnished;
            }
            return Status;
        }


    }
}
