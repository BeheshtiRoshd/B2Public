﻿namespace Bwin.DomainClasses.Games
{
    public enum GameStatus
    {
        GameIsOpen,
        GameIsAboutToStart,
        GameIsCancelled,
        GameIsAbort,
        GameIsRunnig,
        GameIsFnished,
        GameIsAccomplished

    }
}
