﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Bwin.DomainClasses.Base;

namespace Bwin.DomainClasses.Games
{
    public class Team:BaseEntity
    {
        [StringLength(70)]
        public String Name { get; set; }
    }
}
