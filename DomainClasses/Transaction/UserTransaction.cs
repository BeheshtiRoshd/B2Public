﻿using Bwin.DomainClasses.Base;
using Bwin.DomainClasses.Users;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bwin.DomainClasses.Transaction
{
    public class UserTransaction:BaseEntity
    {
        public int Price { get; set; }
        [StringLength(200)]
        public string Reason { get; set; }
        public Transactor Transactor { get; set; }

        [ForeignKey("AdminId")]
        public User Admin { get; set; }
        public int? AdminId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        public int UserId { get; set; }
    }

    public enum Transactor
    {
        ByCharging,
        ByAdmin,
        GameWasCancelled,
        UserGameAborted,
        TakePartInGame
    }
}
