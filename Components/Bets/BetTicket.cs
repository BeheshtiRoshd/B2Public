﻿using System.Collections.Generic;
using Bwin.DomainClasses.Games;
using Bwin.DomainClasses.Games.Basket;
using System;

namespace Bwin.Components.Bets
{

    /// <summary>
    ///   each bet item user choose to put price on
    ///   bet ticket will be inherited by each bet group
    /// </summary>
    public abstract class BetTicket
    {
        protected string Title { get; set; }
        protected BetKind BetKind { get; set; }
        protected GameType GameType { get; set; }

        /// <summary>
        /// tips has index as key to detect
        /// and string to show it to user on ui
        /// </summary>
        protected Dictionary<int,string> Tips { get; set; }
        Dictionary<int, Func<Game, bool>> checkers;
        protected void AddTip(int index,string tipText)
        {
            if (Tips == null)
                Tips = new Dictionary<int, string>();
                Tips.Add(index,tipText);
        }

        /// <summary>
        /// according to each tip you define
        /// you need to checker to check user chosen was right or not
        /// </summary>
        /// <param name="index">tip index</param>
        /// <param name="func">the check code</param>
        protected void AddChecker(int index, Func<Game,bool> func)
        {
            if (checkers == null)
                checkers = new Dictionary<int, Func<Game, bool>>();
            checkers.Add(index, func);
        }


        /// <summary>
        /// a method to run check
        /// </summary>
        /// <param name="game"></param>
        /// <param name="gameItem"></param>
        /// <returns></returns>
        public bool RunCheckers( GameItem gameItem)
        {
            return checkers[gameItem.TipIndex](gameItem.Game);
        }

    }
}
