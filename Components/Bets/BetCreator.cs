﻿using Bwin.DomainClasses.Games;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bwin.Components.Bets
{

    /// <summary>
    /// have all bet tickets implementation
    /// </summary>
    public static class BetCreator
    {
        static Dictionary<BetKind, BetTicket> tickets;

      
        static BetCreator()
        {
            tickets = new Dictionary<BetKind, BetTicket>();
            tickets.Add(BetKind.Goals, new GoalsBet());
            tickets.Add(BetKind.EvenOdd, new EvenOddBet());
        }

        /// <summary>
        /// return betTicket based on bet kind
        /// </summary>
        /// <param name="betKind"></param>
        /// <returns></returns>
            public static BetTicket GetTicket(BetKind betKind)
            {
                    return tickets[betKind];
            }
    }
}
