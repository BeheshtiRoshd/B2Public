﻿using Bwin.DomainClasses.Games;

namespace Bwin.Components.Bets
{
    public class EvenOddBet : BetTicket
    {

        public EvenOddBet()
        {
            Title = "تعداد گل";
            GameType = GameType.Soccer;
            BetKind = BetKind.EvenOdd;
            AddTip(0, "زوج");
            AddTip(1, "فرد");



            AddChecker(0, delegate (Game game) {
                return (game.HostScore + game.GuestScore) % 2 == 0;
            });
            AddChecker(1, delegate (Game game) {
                return (game.HostScore + game.GuestScore) % 2 == 1;
            });

        }
    
    }
}
