﻿using Bwin.DomainClasses.Games;
using Bwin.DomainClasses.Games.Basket;

namespace Bwin.Components.Bets
{
    public class GoalsBet:BetTicket
    {
       
        public GoalsBet()
        {
            Title = "برد";
            GameType = GameType.Soccer;
            BetKind = BetKind.Goals;
            AddTip(0,"برد میزبان");
            AddTip(1,"نتیجه برابر");
            AddTip(2,"برد مهمان");

            
          
            AddChecker(0,delegate (Game game){
                return game.HostScore > game.GuestScore;
            });
            AddChecker(1, delegate (Game game) {
                return game.HostScore == game.GuestScore;
            });
            AddChecker(2, delegate (Game game) {
                return game.HostScore < game.GuestScore;
            });
        }


        
    }
}
