﻿using Bwin.DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bwin.Services.Interface
{
    public interface IUserLogServices
    {
        void AddLoginLog(LoginLog loginLog);
        void AddLoginLog(TryLog loginLog);

        List<LoginLog> GetTryLogs(int pageNumber, int pageSize);
        List<TryLog> GetLoginLogs(int pageNumber, int pageSize);
    }
}
