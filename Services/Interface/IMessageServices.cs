﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bwin.DomainClasses.Messages;

namespace Bwin.Services.Interface
{
    public interface IMessageServices
    {
        void AddNewTopic(Topic topic);
        void AddNewMessage(Message message);

        List<Topic> GetTopics(int pageNumber, int pageSize, bool open = true);
        List<Topic> GetAllTopics(int pageNumber, int pageSize);
        List<Message> GetLatestMessages(int topicId,int pageNumber, int pageSize);

    }
}
