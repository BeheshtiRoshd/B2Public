﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bwin.DomainClasses.Transaction;
using Bwin.DomainClasses.Users;

namespace Bwin.Services.Interface
{
    public interface ITransactionServices
    {
            void AddNewUserTransaction(UserTransaction userTransaction);
            List<UserTransaction> GetUserTransactions(int userId, int pageNumber, int pageSize);
            List<UserTransaction> GetUserTransactions(int pageNumber, int pageSize);
        List<UserTransaction> GetUserTransactions(DateTime startTime,DateTime endTime);

        /// <summary>
        /// sum of users repos
        /// </summary>
        /// <returns></returns>
        long GetWholeRepositry();

        /// <summary>
        /// order by maximum of repo
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        List<User> TopRepositories(int count = 10);

        
    }
}
