﻿using Bwin.DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bwin.Services.Interface
{
    public interface IUserServices
    {
        void AddUser(User user);
        void AddUserRole(UserRole role);

        User GetUser(int userId);
        User GetUserAlongWithRole(int userId);
        User GetUser(string username);
        User GetUser(string username,string password);
        List<User> GetUsers(int pageNumber, int pageSize);
        List<User> GetUsers(string name);

        List<UserRole> GetRoles(int userId);
        void RemoveRole(UserRole role);
    }
}
