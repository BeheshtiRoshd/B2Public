import { Component } from '@angular/core';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css', './vendor-rtl.min.css', './elephant-rtl.min.css', './application-rtl.min.css', './demo-rtl.min.css']
})
export class AppComponent {
}
